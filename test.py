import unittest
import price

class TestPrice(unittest.TestCase):
    def test_compute_price(self):
        self.assertEqual(price.comput_price("Orange"), 140)

if __name__ == '__main__':
    unittest.main()
